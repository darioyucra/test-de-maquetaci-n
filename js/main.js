"use strict";
window.addEventListener('load', function(){
    var btnMenu = document.querySelector('#menuAbrir');
    var menuOff = 'contenedorMenu contenedorMenuInicial menuInactivo';
    var menuOn = 'contenedorMenu contenedorMenuInicial menuActivo';
    var menu = document.querySelector('#contenedorMenu');

    btnMenu.addEventListener('click', function(){
        if(menu.className == menuOff){
            menu.className = menuOn;
        }else{
            menu.className = menuOff;
        }
    });
});